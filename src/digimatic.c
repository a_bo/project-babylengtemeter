#include "digimatic.h"
#include <math.h>
#define bitWrite(value, bit, bitvalue) (bitvalue ? bitSet(value, bit) : bitClear(value, bit))
#define bitSet(value, bit) ((value) |= (1UL << (bit)))
#define bitClear(value, bit) ((value) &= ~(1UL << (bit)))

uint8_t mydata[14];

void setupDigimatic(Digimatic *dgm, uint16_t data_tmp, uint16_t clk_tmp, uint16_t req_tmp, GPIO_TypeDef *dataPort_tmp, GPIO_TypeDef *clkPort_tmp, GPIO_TypeDef *reqPort_tmp)
{
	dgm->clk = clk_tmp;
	dgm->data = data_tmp;
	dgm->req = req_tmp;
	dgm->dataPort = dataPort_tmp;
	dgm->clkPort = clkPort_tmp;
	dgm->reqPort = reqPort_tmp;
	HAL_GPIO_WritePin(reqPort_tmp, req_tmp, GPIO_PIN_SET); //rq pin is default high
}

void prepareNextRead(Digimatic *dgm) {
	HAL_GPIO_WritePin(dgm->reqPort, dgm->req, GPIO_PIN_RESET);
}

/* @brief: Reads from digimatic and stores 
		result in struct that's passed
*	Returns whether read was successful
*	Actual measurement is stored inside
*	Digimatic instance
*/
bool readMeter(Digimatic *dgm)
{
	uint16_t reqPin = dgm->req;
	uint16_t clkPin = dgm->clk;
	uint16_t dataPin = dgm->data;

	GPIO_TypeDef *reqPort = dgm->reqPort, *clkPort = dgm->clkPort, *dataPort = dgm->dataPort;

	for (int i = 0; i < 13; i++)
	{
		int k = 0;

		for (int j = 0; j < 4; j++)
		{
			long millisStart = HAL_GetTick();
			while (HAL_GPIO_ReadPin(clkPort, clkPin) == 0)
			{
				//If its stuck waiting for MAX_DURATION return with failure (0)
				//Display will show ??.?cm as measurement incase of failure
				if(HAL_GetTick() - millisStart >= MAX_DURATION) {
					return 0;
				}
			} // hold until clock is high
			while (HAL_GPIO_ReadPin(clkPort, clkPin) == 1)
			{
				if(HAL_GetTick() - millisStart >= MAX_DURATION) {
					return 0;
				}
			}
			bitWrite(k, j, (HAL_GPIO_ReadPin(dataPort, dataPin) & 0x1)); // read bits and reverse order
		}
		mydata[i] = k;
	}
	long num;
	double measurement;
	char buf[7];

	// Read mydata array into a char array buffer (buf) and pad with 0, for conversion to a long
	for (int lp = 0; lp < 6; lp++)
		buf[lp] = mydata[lp + 5] + '0';

	// Convert buf to a long
	num = atol(buf);

	// If the measurement was negative make num negative.
	if (mydata[4] == 8)
		num = -num;

	// Convert num to a value with the decimal point in place
	measurement = (double)num * (double)pow(10, -(mydata[11]));
	//Store measurement in struct, method returns only success or fail
	(dgm->measurement).length = measurement;

	// Store measurement in struct in in/mm
	if (mydata[12])
		(dgm->measurement).inMillimeters = 0;
	else
		(dgm->measurement).inMillimeters = 1;

	HAL_GPIO_WritePin(reqPort, reqPin, GPIO_PIN_SET); //write 1 (request pin is default high)
	return 1;
}

//1 decimal precision
//With measurement being in cm
static const uint8_t precision = 1;

// Converts double to string
/* 	@param lengte: buffer that holds
 * 	measurement in string form
 * 	@param *dgm: pointer to
 * 	Digimatic instance which contains
 * 	last measurement
 * 
 * 	Reason that sprintf isnt used is that
 *	printf floating support is disabled
 *	in library
 **/
void getStr(char *lengte, Digimatic *dgm)
{
	Measurement m = (dgm->measurement);
	double number = m.length;

	// Handle negative numbers
	bool isPositive = true;
	number += OFFSET_MEASUREMENT;

	if (number < 0.0)
	{
		isPositive = false;
		number = -number;
	}

	//Omzetten naar cm
	if (m.inMillimeters)
	{
		number /= 10;
	} else {
		//geen ondersteuning voor inch
	}

	// Extract integer part
	unsigned long int_part = (unsigned long)number;

	// Extract part after comma
	double remainder = number - (double)int_part;

	//Number composed of x digits after comma (precision)
	int remainderPart = remainder * pow(10.0, precision);
	//Number composed of x + 1 digits after comma (precision)
	int sremainderPart = remainder * pow(10.0, precision + 1);
	
	//1 digit after the 'precision' amount of digits
	int lastDigit = sremainderPart - (remainderPart * 10);

	//If number after comma is zero
	if(remainderPart == 0) {
		lastDigit = sremainderPart;
	}

	//From 5 and above round upwards
	if(lastDigit >= 5) {
		remainderPart++;
	}
	
	//Using a sign char with %c is unstable
	if (isPositive || (int_part == 0 && remainderPart == 0)) {
		sprintf(lengte, "%d.%01d%s", (int)int_part, (int)remainderPart, m.inMillimeters ? "cm" : "in");
	} else {
		sprintf(lengte, "-%d.%01d%s", (int)int_part, (int)remainderPart, m.inMillimeters ? "cm" : "in");
	}
}
