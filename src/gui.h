#ifndef GUI_H
#define GUI_H

#include "u8g2/u8g2.h"
#include <stm32l0xx_hal.h>
#include "digimatic.h"

#define DEFAULT_FONT u8g2_font_profont22_tr
//#define DEBUG_MODE

void setupGui(u8g2_t *u8g2);
void setDefaultFont(u8g2_t *u8g2);

void drawMeter(u8g2_t *u8g2, Digimatic *dgm);
void drawBattery(u8g2_t *u8g2, I2C_HandleTypeDef *handle);

#endif