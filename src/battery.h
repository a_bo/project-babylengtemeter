#ifndef BATTERY_H
#define BATTERY_H

#include <stm32l0xx_hal.h>
#include <stdbool.h>
#include "u8g2/u8g2.h"

bool isChipAvailable(I2C_HandleTypeDef *handle);
int getSOC(I2C_HandleTypeDef *handle);
int readAndPrint(u8g2_t *u8g2, I2C_HandleTypeDef *handle);
void applyCompensation(I2C_HandleTypeDef *handle, float temp);

#endif