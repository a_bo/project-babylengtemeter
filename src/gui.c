#include "gui.h"
#include "battery.h"
#include <stdbool.h>

static int displayWidth;
static int displayHeight;

static const int batteryLength = 25;
static const int batteryPlugWidth = 2;
static const int batteryHeight = 15;
static const int batteryParts = 5;

static int batteryUpperLeftY;
static int batteryLeftX;

static const int meanFontHeight = 13;

bool blinkBattery = false;

/*@brief: Initializes variables required for drawing gui
*
* @param *u8g2: contains instance for GFX library
*/
void setupGui(u8g2_t *u8g2)
{
  displayWidth = u8g2_GetDisplayWidth(u8g2);
  displayHeight = u8g2_GetDisplayHeight(u8g2);
  batteryLeftX = (displayWidth / 2) - (batteryLength / 2) - batteryPlugWidth;
  batteryUpperLeftY = displayHeight - (displayHeight / 3);
  u8g2_ClearBuffer(u8g2);
  u8g2_SetFont(u8g2, DEFAULT_FONT);
}

static char lengthCharArr[20];

//XBM Bitmap of an X cross used to show that battery level is critical
//Use of symbols is not possible due to flash space
//shortage when including a font with symbols alongisde
//currently used font
const uint8_t cross[] = {
   0x00, 0x00, 0x0e, 0x0e, 0x1e, 0x0f, 0xbe, 0x0f, 0xfc, 0x07, 0xf8, 0x03,
   0xf0, 0x01, 0xf8, 0x03, 0xfc, 0x07, 0xbe, 0x0f, 0x1e, 0x0f, 0x0e, 0x0e,
   0x00, 0x00 
};

/*@brief: Initializes variables required for drawing gui
*
* @param *u8g2: contains instance for GFX library
*/
void drawBattery(u8g2_t *u8g2, I2C_HandleTypeDef *handle)
{
  bool success = isChipAvailable(handle);

  int batteryPercentage = getSOC(handle);
  if(!success) {
    batteryPercentage = -1;
  }

  //Display percentage in numbers on screen
  #ifdef DEBUG_MODE
    char socArray[10];
    if(isChipAvailable(handle)) {
      sprintf(socArray, "%d%%", batteryPercentage);
    } else {
      //Read failed
      sprintf(socArray, "XX%%");
    }
    u8g2_DrawStr(u8g2, 75, 42, socArray);
    //Battery percentage cycles by itself so we know MCU is running and not stuck
    batteryPercentage = (HAL_GetTick() % 20000) / 200;
  #endif

  if (batteryPercentage < 8)
  {
    // blink each 1.5 seconds and/or 3 seconds long with 50% duty cycle
    if (HAL_GetTick() % 3000 >= 1500 || !success)
    {                             
      //Draw cross symbol in form of an XMB bitmap  
      u8g2_DrawXBM(u8g2, batteryLeftX + 6, batteryUpperLeftY + 2 - 1, 13, 13, cross); //draw an X symbol
    }
  }

  //Draw frame for the battery icon
  u8g2_DrawFrame(u8g2, batteryLeftX, batteryUpperLeftY, batteryLength, batteryHeight);
  u8g2_DrawBox(u8g2, batteryLeftX + batteryLength, batteryUpperLeftY + 2, 2, batteryHeight - 4);
  double partlength = (double)(batteryLength - 4) / (double)batteryParts;

  int percentageParts = batteryPercentage / (100 / batteryParts);
  for (int i = 0; i < percentageParts; i++)
  {
    //Draw battery parts (batteryLeft X + (distance between battery parts))
    u8g2_DrawBox(u8g2, batteryLeftX + 3 + (i * partlength), batteryUpperLeftY + 3, partlength - 1, batteryHeight - 6);
  }
}

void drawMeter(u8g2_t *u8g2, Digimatic *dgm)
{
  bool success = readMeter(dgm);
  if (success) {
    getStr(lengthCharArr, dgm);
  } else {
    sprintf(lengthCharArr, "??.?cm");
  }

  //Middenpunt scherm berekenen
  int startX = (displayWidth - u8g2_GetStrWidth(u8g2, lengthCharArr)) / 2;
  int startY = (displayHeight / 2) - meanFontHeight; //11 is

  u8g2_DrawStr(u8g2, startX, startY, lengthCharArr);
}