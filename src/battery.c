#include "battery.h"

//I2C Address for MAX17408g chip
#define I2C_ADDR_BAT 0x36
//I2C Register containing State Of Charge
#define MEM_ADDR_SOC 0x04
//I2C Register containing CMD Register
#define MEM_ADDR_RESET 0xFE
//Register value that's written to reset IC
#define RESET_PAYLOAD 0x54

/*
* the HAL wants a left aligned i2c address
* &hi2c1 is the handle
* (uint16_t)(address<<1) is the i2c address left aligned
* this address is used when using hal i2c functions
*/
const uint16_t address = (uint16_t) (I2C_ADDR_BAT << 1);
const uint8_t socAddr = MEM_ADDR_SOC;
const uint8_t resetData[1] = {RESET_PAYLOAD};
const uint8_t base_comp = 0x97;
const float defaultTemp = 20;

bool isChipAvailable(I2C_HandleTypeDef *handle) {
    HAL_StatusTypeDef result = HAL_I2C_IsDeviceReady(handle, address, 2, 20);
    return result == HAL_OK;
}

//Reset is required as otherwise the SOC
//remains the same, it's only updated after a reset
void resetChip(I2C_HandleTypeDef *handle) {
    //No timeout as there's no ack after reset command
    HAL_I2C_Mem_Write(handle, address, MEM_ADDR_RESET, I2C_MEMADD_SIZE_8BIT, resetData, 1, 1);
}

int getSOC(I2C_HandleTypeDef *handle) {
    //battery chip sends back 2 bytes
    uint8_t data[2];
    
    //use specific i2c function for reading/writing to registers/memory otherwise wont work
    HAL_StatusTypeDef status = HAL_I2C_Mem_Read(handle, address, socAddr, I2C_MEMADD_SIZE_8BIT, data, 2, 500);
    if(status != HAL_OK) {
        return -1;
    }

    //convert to float
    float decimal = data[1] / 256.0;
    decimal += data[0];

    //Reset chip so SOC is updated
    resetChip(handle);
    return decimal;
}

static uint16_t mergeBytes(uint8_t* data) {
    return data[0] << 8 | data[1];
}

//temperature compensation (not needed)
void applyCompensation(I2C_HandleTypeDef *handle, float temp) {
    uint8_t rcomp;
    if (temp > 20.0) {
        rcomp = base_comp + (temp - 20.0) * -0.5;
    } else {
        rcomp = base_comp + (temp - 20.0) * -5.0;
    }

    uint8_t regAddr = 0x0C;
    uint8_t data[2];
    HAL_I2C_Mem_Read(handle, address, regAddr, I2C_MEMADD_SIZE_8BIT, data, 2, 500);
    uint16_t value = mergeBytes(data);

    value &= 0x00FF;
    value |= rcomp << 8;
    data[0] = (uint8_t) (value >> 8);
    data[1] = (uint8_t) (value & 0xFF);
    HAL_I2C_Mem_Write(handle, address, regAddr, I2C_MEMADD_SIZE_8BIT, data, 2, 200);
}