#ifndef DIGIMATIC_H
#define DIGIMATIC_H

#include <stm32l0xx_hal.h>
#include <stdbool.h>
//#include <string>
#define MAX_DURATION 300 //Amount of ms a reading may take
//Offset van de behuizing in mm
#define OFFSET_MEASUREMENT 178

typedef struct Measurement {
  double length;
  uint8_t inMillimeters;
} Measurement;

typedef struct Digimatic
{
	uint16_t data,clk,req;
	GPIO_TypeDef *dataPort,*clkPort,*reqPort;
	Measurement measurement;
} Digimatic;

void setupDigimatic(Digimatic *dgm, uint16_t data_tmp, uint16_t clk_tp, uint16_t req_tmp, GPIO_TypeDef *dataPort_tmp, GPIO_TypeDef *clkPort_tmp, GPIO_TypeDef *reqPort_tmp);

void prepareNextRead(Digimatic *dgm);

bool readMeter(Digimatic *instance);

void getStr(char* array, Digimatic *dgm);

#endif